$('.accordion-navigation a').click(function(e) {
    e.preventDefault();

    if (!$(this).parent().hasClass('active')) {
    	$('.accordion-navigation').removeClass('active');
    	$('.accordion .content').removeClass('active');
        $(this).parent().toggleClass('active');
        $(this).siblings('.content').toggleClass('active');
    } else {
        $(this).parent().removeClass('active');
        $(this).siblings('.content').removeClass('active');
    }
});

