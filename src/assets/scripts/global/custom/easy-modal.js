/* Easy Modal

    Todo:
    - Lock focus to the modal, prevent tabbing
      outside the modal when it exists.
*/

function easyModal() {

  'use strict';

  // define variables
  // modal, links that call the modals, & the modal screen.
  var modal = document.querySelectorAll('.modal'),
      modalTriggers = document.querySelectorAll('[href*="modal"]'),
      allNodes = document.querySelectorAll("*"),
      screen = '<div class="screen" tabindex="-1" aria-hidden="true"></div>',
      lastFocus;

  // gather up the modals on the page and place them
  // at the bottom of the page.
  for (var i = 0; i < modal.length; i++) {
    document.body.appendChild(modal[i]);
    modal[i].setAttribute('aria-hidden', 'true');
  }
  // place the screen before the first modal in the DOM.
  modal[0].insertAdjacentHTML('beforebegin', screen);

  // INITIALIZE MODALS
  // find all modal triggers on the page.
  // bind them to an eventListener for locateModal.
  for (var j = 0; j < modalTriggers.length; j++) {
    var triggers = modalTriggers[j];
    triggers.addEventListener('click', locateModal, false);
  }


  // define when the screen is active
  // apply an eventListener to the screen for closing
  var activeScreen = document.querySelector('.screen');
  activeScreen.addEventListener ('click', function(){closeModal(activeScreen);});
  document.addEventListener('keydown', keydownModal, false);

  function locateModal(e) {
    // find the item that was clicked.
    var clickedItem = e;
    // locate the href attribute of the element that was clicked and strip out the "modal-" part.
    var modalName = e.target.getAttribute("href").substring(6).toLowerCase();
    // locate the modal with the "data-modal-name" attribute that
    // matches the anchor's href value that was previously extracted.
    var selectorName = "[data-modal-name=\"" + modalName + "\"]";
    // grab the modal that matches the "selectorName" previously found.
    var modalFound = document.querySelector(selectorName);
    window.modalFound = modalFound;
    // prevent any click on the anchor from taking us off
    // the page to a broken link
    e.preventDefault();

    // If the anchor's href is "modal-close" or the
    // esc key was pressed, then just close any open
    // modals and stop there. Otherwise, close any open
    // modals and open the modal that was referenced
    // in the anchor.

    if (modalName === 'close') {
      closeModal(activeScreen);
    } else {
      closeModal(activeScreen);
      openModal(modalFound, activeScreen);
    }
  }

  function keydownModal(e) {
    if ( e.keyCode === 27 ) {
      closeModal(activeScreen);
    }
  }

  // Restrict Focus to modal only
  function focusRestrict ( e ) {
    if (typeof modalFound === 'undefined' || modalFound === null) {
      console.log('no modalFound');
      return;
    } else if ( typeof modalFound !== 'undefined' && !modalFound.contains( e.target ) ) {
      e.stopPropagation();
      modalFound.focus();
    }
  }

  // OPEN
  // Set "aria-hidden = false" on the activated modal & the screen.
  function openModal(modalFound, activeScreen) {
    //lastFocus = document.activeElement;
    modalFound.setAttribute('aria-hidden', 'false');
    activeScreen.setAttribute('aria-hidden', 'false');
    modalFound.setAttribute('tabindex', '0');

  }

  // Apply focusRestrict to the current modal.
  for (i = 0; i < allNodes.length; i++) {
    allNodes.item(i).addEventListener('focus', focusRestrict);
  }

  // CLOSE
  // Set "aria-hidden = true" on all modals & the screen.
  function closeModal(activeScreen) {
    for (var i = 0; i < modal.length; i++) {
      modal[i].setAttribute('aria-hidden', 'true');
      modal[i].setAttribute('tabindex', '-1');
    }
    activeScreen.setAttribute('aria-hidden', 'true');
  }
}


window.onload = function () {
  easyModal();
};