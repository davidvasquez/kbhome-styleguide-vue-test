// Place all globally accessible script initializations in this file.

$(document).ready(function () {

    $(function () {
        App.postInit();
    });

	$(".is-sticky").sticky({ topSpacing: 20, bottomSpacing: 200 });

});