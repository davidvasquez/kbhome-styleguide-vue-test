(function () {



    $(document).on("change", "#m-footer select.states", stateChanged);
	$(document).on("change", "#m-footer select.counties", regionChanged);
	$(document).on("click", "#m-footer input.m-explore-btn:submit", exploreRegion);



    var selectedRegion = "";
    var inInit = false;



    var stateSel = $("#m-footer select.states", this);
    var state = $("#m-footer select.states")
		.not(stateSel)
		.first()
		.val();

    if (!state || state == "")
        return;

    inInit = true;
    // select the state
    stateSel
		.val(state)
		.change();

    // select the region
    stateSel
		.closest("form")
		.find(".counties select[data-for-state='" + state + "']")
		.val(selectedRegion)
		.change();

    inInit = false;



    function stateChanged() {
        var state = $(this).val();

        $(".m-region-counties").hide();
        $("select[data-for-state='" + state + "']").val('');
        $("#m-footer input.m-explore-btn:submit").disable(true);
        $("select[data-for-state='" + state + "']").closest("div.m-region-counties").show();

		/*
        if (state != '' && !inInit)
            $(document).trigger("analytics.event", ["Select", "Body Link", "Explore Area Map:" + state + ":{u}"]);
		*/
    }



    function regionChanged() {
        $("#m-footer input.m-explore-btn:submit").disable($(this).val() == "");
        selectedRegion = $(this).val();

        var selectedState = $("select.states").val();

		/*
        var regionname = $(":selected", this).jqmData("tracking");
        if (regionname != '' && !inInit)
            $(document).trigger("analytics.event", ["Select", "Body Link", "Explore Area Map:" + selectedState + ":" + regionname + ":{u}"]);
		*/
    }



    function exploreRegion() {
        var selectedState = $("select.states").val();
        var selectedRegionName = $("select[data-for-state='" + selectedState + "'] option:selected").text();
        /*
        $(document).trigger("analytics.event", ["Body Link", "Click", "Explore Area Map:" + selectedState + ":" + selectedRegionName + ":{u}"]);
		*/
        window.location.href = selectedRegion;       
        return false;
    }




})();