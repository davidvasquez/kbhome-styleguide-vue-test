$(function () {

    App.init();

});





//Namespace
var App = {

    init: function () {


        //--Slideshow control prevent # navigation
        $(".flip").on("click", function () { return false; });


        App.navFunctions.init();
        App.searchFunctions.init();
        App.mortgageCalc.init();
        App.tablesorter.init();
        App.contactSearchFunctions.init();
        App.communities.init();


    },

    // called as the last thing after page loads
    postInit: function () {

    },

    navFunctions: {

        communitySelector: null,

        init: function () {
            var mainComSlideFunc = App.navFunctions.closeAllNav;

            /* copy communities nav to home nav */
            $("div.statesHome").append($("div.states ul").clone());
            $("div.countiesHome").append($("div.counties ul").clone());


            /* Nav animations */
            //COMMUNITES & FLOORPLANS CLICK
            $('li.communities > a').click(function (e) {
                App.navFunctions.customizeSlide();
                $('.recentviewed').slideDown();

                //show floor plans in community
                var communityLength = $(".community ul.bList").children().length
                if (communityLength > 0) {
                    $(".community").css({ 'height': ($('.community ul .listing').outerHeight() * 2) + 28 });
                    $('.community').fadeIn('fast').animate({ 'width': '230px' }, {
                        duration: 200, queue: false, ease: "easeOutCubic",
                        complete: function () {
                            $('.community ul').fadeIn('fast');
                            $('.community a').click(function () {
                                $('.community ul').fadeOut('fast', function () {
                                    $('.community').animate({ 'width': '0' }, { duration: 200, queue: false, ease: "linear", complete: function () { $('.recentviewed').slideUp(); } }).fadeOut('fast');
                                });
                            });
                        }
                    });
                    App.navFunctions.communitySelector = $(this).attr('class');
                    //return false;
                }





                //Bind a document click to click anywhere close the nav.
                $(document).bind('click', function () {
                    App.navFunctions.closeAllNav();
                    $(this).unbind('click');
                });

                return false;
            });

            $('.bto li a').click(function () {
                // TODO Fire off page action.
                App.navFunctions.customizeSlide();

                //Bind a document click to click anywhere close the nav.
                $(document).bind('click', function () {
                    //App.navFunctions.closeAllNav();
                    $('.bto').slideUp();
                    $(this).unbind('click');
                });

            });



            //close nav on click.
            $('.counties li a, .countiesHome li a').click(function () {
                App.navFunctions.countiesClose(mainComSlideFunc);
                $(document).bind('click', function () {
                    App.navFunctions.closeAllNav();
                    $(this).unbind('click');
                });
            });

            // Recent viewed clicked
            $('#nav .recent').click(function () {
                App.navFunctions.countiesClose();
            });

            $('#comNames').click(function () {
                App.navFunctions.communityClose();
                $('.community').stop(true, true);
                $('.bto').slideUp();
                $('.bto').stop(true, true);

                // Fade in states ul
                $('.states').fadeIn('fast').animate({ 'width': '114px' }, {
                    duration: 200, queue: false, ease: "easeOutCubic",
                    complete: function () {
                        //set height of navbox to ensure proper spacing
                        $(".navBox").css({ 'height': $('.states').height() + 20 });
                        $('.states li a').click(function () {
                            if ($(this).attr('href') != '#') {
                                return true;
                            }
                            var state = $(this).attr('data-state');
                            $('.counties >ul').hide();
                            var selCounty = $('.counties >ul[data-for-state="' + state + '"]');
                            selCounty.show();
                            $('.counties').fadeIn('fast').animate({ 'width': '185px' }, { duration: 200, queue: false, ease: "easeOutCubic" });
                            return false;
                        });
                    }
                });
                App.navFunctions.communitySelector = $(this).attr('id');
                return false;
            });


            $('#comNamesHome').click(function () {
                App.navFunctions.communityClose();
                $('.community').stop(true, true);
                $('.bto').slideUp();
                $('.bto').stop(true, true);

                // Fade in states ul
                $('.statesHome').fadeIn('fast').animate({ 'width': '114px' }, {
                    duration: 200, queue: false, ease: "easeOutCubic",
                    complete: function () {
                        //set height of navbox to ensure proper spacing
                        $(".navBoxHome").css({ 'height': $('.statesHome').height() + 20 });
                        $('.statesHome li a').click(function () {
                            if ($(this).attr('href') != '#') {
                                return true;
                            }
                            var state = $(this).attr('data-state');
                            $('.countiesHome >ul').hide();
                            var selCounty = $('.countiesHome >ul[data-for-state="' + state + '"]');
                            selCounty.show();
                            $('.countiesHome').fadeIn('fast').animate({ 'width': '185px' }, { duration: 200, queue: false, ease: "easeOutCubic" });
                            return false;
                        });
                    }
                });
                App.navFunctions.communitySelector = $(this).attr('id');
                $(document).bind('click', function () {
                    App.navFunctions.closeAllNav();
                    $(this).unbind('click');
                });
                return false;
            });

            $('#comNamesStateHome').click(function () {
                App.navFunctions.communityClose();

                var state = $(this).attr('data-state');
                $('#stateHomeCounties >ul').hide();
                var selCounty = $('#stateHomeCounties >ul[data-for-state="' + state + '"]');
                selCounty.show();
                $('#stateHomeCounties').fadeIn('fast').animate({ 'width': '185px' }, { duration: 200, queue: false, ease: "easeOutCubic" });

                App.navFunctions.communitySelector = $(this).attr('id');
                $(document).bind('click', function () {
                    App.navFunctions.closeAllNav();
                    $(this).unbind('click');
                });
                return false;
            });


            $('.community a').click(function () {
                // TODO Fire off page action.
                App.navFunctions.communityClose(mainComSlideFunc);
            });

            //BUILT TO ORDER CLICK
            $('li.custom a.builtToOrderSection').click(function (e) {

                App.navFunctions.closeAllNav();

                $('.bto').slideDown();

                $(document).bind('click', function () {
                    //App.navFunctions.closeAllNav();
                    $('.bto').slideUp();
                    $(this).unbind('click');
                });

                return false;
            });




            $('a#action-menu').click(function () {
                $(this).parent().children('.action-dropdown').toggle();
                return false;
            });




        },
        //Slide communities and floorplans link back up.
        comSlide: function () {
            $('.recentviewed').slideUp();

            if ($('.community ul').is('visible')) {
                $('.community ul').fadeOut('fast', function () {
                    $('.community').animate({ 'width': '0' }, { duration: 200, queue: false, ease: "linear", complete: function () { $('.recentviewed').slideUp(); } }).fadeOut('fast');
                });
            }
        },
        closeAllNav: function () {
            if (App.navFunctions.communitySelector == 'recent') {
                App.navFunctions.communityClose(App.navFunctions.comSlide);
            } else if (App.navFunctions.communitySelector == 'comNames' || App.navFunctions.communitySelector == 'comNamesHome' || App.navFunctions.communitySelector == 'comNamesStateHome') {
                App.navFunctions.countiesClose(App.navFunctions.comSlide);
            } else { App.navFunctions.communityClose(App.navFunctions.comSlide); }
        },



        //Slide built to order
        customizeSlide: function () { $('.bto').slideUp(); },

        countiesClose: function (func) {

            $('.counties, .countiesHome').animate({ 'width': '0' }, {
                duration: 200, queue: false, ease: "linear",
                complete: function () {
                    $('.states, .statesHome').animate({ 'width': '0' }, {
                        duration: 200, queue: false, ease: "linear",
                        complete: func
                    }).fadeOut();
                    $(".navBox, .navBoxHome").css({ 'height': '0' });
                }
            }).fadeOut();
            App.navFunctions.communitySelector = null;
        },


        communityClose: function (func) {
            $('.community ul').fadeOut('fast', function () {
                $('.community').animate({ 'width': '0' }, { duration: 200, queue: false, ease: "linear", complete: func }).fadeOut('fast');

            });
            App.navFunctions.communitySelector = null;
        }
    },


    searchFunctions: {
        init: function () {

            //Search Placeholder Text
            setTimeout(function () {
                $('input.gsc-input').attr('placeholder', 'Search');
            }, 1000);

            //Trigger Search on Enter
            $('.section-03 input, #mSearchbar').keypress(function (e) {
                if (e.which == 13) {
                    var search = $(this).val();
                    var domain = window.location.host;
                    location.assign("http://" + domain + "/search/?q=" + search.replace(/[<>&?*.()]/g, ""));
                }
            });

            //Enable Mobile Search
            $('.goSearch').click(function () {
                var search = $(this).prev('input').val();
                var domain = window.location.host;
                location.assign("http://" + domain + "/search/?q=" + search.replace(/[<>&?*.()]/g, ""));
            });

        }
    },


    mortgageCalc: {

        init: function () {



            try {
                //temporary - switch back to fixed number later
                //var mortgageAmount = $('#mCalculator b').html().replace(/[Amount:$, ]/g, '');
                var mortgageAmount = $('#mCalculator #amount').val().replace(/[Amount:$, ]/g, '');
                var mortgageRate = $('#rate').val().replace('%', '');
            } catch (err) {

                App.mortgageCalc.financePage();
                return false;

            }

            var downpay = 0;
            var showDisclaimer = '';

            //temporary - switch back to fixed number later
            //var monthly_payment = App.mortgageCalc.calculate_payment(mortgageAmount, downpay, mortgageRate, taxes, insurance, annualtax, fha, cll)

            $('#submitRate').click(function () {
                $(".error").hide();

                showDisclaimer = $.cookie('calc_disclaimer');

                if (showDisclaimer != 'false') {
                    $("#calculatordisclaimer").show();
                    $.cookie('calc_disclaimer', 'false');
                }

                mortgageAmount = $('#amount').val().replace('$', '').replace(/,/g, '');
                downpay = $('#downpayment').val().replace('$', '').replace(/,/g, '');
                mortgageRate = $('#rate').val().replace('%', '');

                if ($.isNumeric(downpay) == false) {
                    $("#dperr").show();
                }

                if ($.isNumeric(mortgageRate) == false) {
                    $("#rateerror").show();
                }

                if ((downpay != "") && (parseInt(downpay) <= parseInt(mortgageAmount))) {
                    //temporary - switch back to fixed number later
                    //monthly_payment = App.mortgageCalc.calculate_payment(mortgageAmount, downpay, mortgageRate, taxes, insurance, annualtax, fha, cll)
                    monthly_payment = App.mortgageCalc.find_payment((mortgageAmount - downpay), mortgageRate);
                    try {
                        ewt.track({ name: communityName, type: 'calculator' });
                        console.info('GTM-SCR003 ewt.track - name: ' + communityName + ', type: savefloorplan');
                    }
                    catch (err) {
                    }
                }
                else {
                    monthly_payment = '$0';
                }
                $('#total-mortgage span').html(monthly_payment)

            });

            $("#closedisclaimer").click(function () {
                $("#calculatordisclaimer").hide();
            });

        },


        financePage: function () {

            try {
                var mortgageAmount = $('#mCalculator #amount').val().replace(/[Amount:$, ]/g, '');
                var mortgageRate = $('#rate').val().replace('%', '');
            } catch (err) {
                return false;
            }

            var downpay = 0;

            var monthly_payment = App.mortgageCalc.find_payment((mortgageAmount - downpay), mortgageRate)





            $('#submitRate').click(function () {
                $(".error").hide();

                mortgageAmount = $('#amount').val().replace('$', '').replace(/,/g, '');
                downpay = $('#downpayment').val().replace('$', '').replace(/,/g, '');
                mortgageRate = $('#rate').val().replace('%', '');

                if (($.isNumeric(downpay) == false) || ($.isNumeric(mortgageAmount) == false)) {
                    $(".error").show();
                }

                if ((downpay != "") && (parseInt(downpay) <= parseInt(mortgageAmount))) {
                    monthly_payment = App.mortgageCalc.find_payment((mortgageAmount - downpay), mortgageRate)
                }
                else {
                    monthly_payment = '$0';
                }
                $('.total span').html(monthly_payment)
            });

        },



        find_payment: function (PR, IN) {
            PR = PR;
            IN = (IN / 100) / 12;
            var PE = 30 * 12

            var PAY = (PR * IN) / (1 - Math.pow(1 + IN, -PE))
            return '$' + App.mortgageCalc.numFormat(Math.ceil(PAY));
        },
        numFormat: function (amount) {
            var delimiter = ",";
            var i = parseInt(amount);
            if (isNaN(i)) { return ''; }
            var origi = i;  // store original to check sign
            i = Math.abs(i);

            var minus = '';
            if (origi < 0) { minus = '-'; } // sign based on original

            var n = new String(i);
            var a = [];

            while (n.length > 3) {
                var nn = n.substr(n.length - 3);
                a.unshift(nn);
                n = n.substr(0, n.length - 3);
            }

            if (n.length > 0) { a.unshift(n); }

            n = a.join(delimiter);

            amount = minus + n;

            return amount;
        },

        calculate_payment: function (P, DP, IN, TR, IR, AAT, FHA, CONF) {
            PR = (P - DP);
            IN = (IN / 100) / 12;
            var PE = 30 * 12
            var MI = App.mortgageCalc.mortgage_insurance(P, FHA, CONF)

            var PI = (PR * IN) / (1 - Math.pow(1 + IN, -PE))
            var M = ((PR * MI) / 12)
            var T = (P * TR / 100 / 12)
            var I = (P * IR / 100 / 12)
            var AT = (AAT / 12)
            var PAY = PI + M + T + I + AT

            return '$' + App.mortgageCalc.numFormat(Math.ceil(PAY));
        },

        mortgage_insurance: function (price, fha, conf) {
            var rate = 0;

            if ((price * .965 * (1 + 0.01)) <= fha) {
                rate = 0.0115;
            }
            else if ((price * .95) <= conf) {
                rate = 0.0094;
            }
            else if ((price * .90) <= conf) {
                rate = 0.0062;
            }

            return rate;
        }
    },


    tablesorter: {

        init: function () {
            try {
                $("#saved-floorplans").tablesorter();
            } catch (err) {
                return false;
            }
        }

    },

    tablesorterBroker: {

        init: function () {
            try {
                $("#broker-communities").tablesorter();
                $("#broker-communities").trigger("update");
            } catch (err) {
                return false;
            }
        }

    },

    contactSearchFunctions: {
        init: function () {
            $('select#area').change(function () {
                var state = $('select#area').val();
                $('.region_info').hide(); //hide the region info if we're switching states
                $("#region option.region").remove(); //remove all the regions
                $('select#hidden_regions option.region_' + state).clone().appendTo('#region'); //grab the regions for this state and add them to the list
            });
            $('select#region').change(function () {
                var region = $('select#region').val();
                $('.region_info').hide();
                $('#region_info_' + region).show();
            });

            $('select#area2').change(function () {
                var state = $('select#area2').val();
                $('.contact_info').hide(); //hide the region info if we're switching states
                $('.contact_info_' + state).show();
            });
        }
    },

    communities: {
        init: function () {
            // if the current page doesn't contain a section with the id of the hash
            // link (i.e. $("#neighborhoods")), then redirect to the community page.
            //$("#nav-page li a").click(function () {
            //    if (!$($(this).attr("href")).length) {
            //        location.href = $(this).closest("ul").attr("data-community") + $(this).attr("href");
            //    }
            //});
        }
    },

    Url: function (path) {
        return (APP_PATH || '') + (path[0] != '/' ? '/' : '') + path;
    }



} //end namespace








var kb = kb || {
    ns: function extend(namespace) {
        var parts = namespace.split('.'),
            parent = this,
            pl,
            i;

        if (parts[0] == 'kb')
            parts = parts.slice(1);

        pl = parts.length;

        for (i = 0; i < pl; i++) {
            //create a node in ns if it doesn't exist
            if (typeof parent[parts[i]] == 'undefined')
                parent[parts[i]] = {};

            parent = parent[parts[i]];
        }

        return parent;
    },
    util: {
        text: {
            formatCurrency: function (number, noSymbol, decimals) {
                decimals = typeof decimals !== 'undefined' ? decimals : 2;
                var negative = '';
                if (number < 0) {
                    negative = '- ';
                    number = number * -1;
                }
                var symbol = noSymbol ? "" : "$";

                return negative + symbol + kb.util.text.formatNumber(number, 0, decimals, true);
            },

            formatNumber: function (number, digits, decimalPlaces, withCommas) {
                number = number ? number.toString() : "0";
                var simpleNumber = '';
                var negative = false;

                // Strips out the dollar sign and commas.
                for (var i = 0; i < number.length; ++i) {
                    if ("0123456789.".indexOf(number.charAt(i)) >= 0)
                        simpleNumber += number.charAt(i);
                    if ("-".indexOf(number.charAt(i)) >= 0)
                        negative = true;
                }

                number = parseFloat(simpleNumber);

                if (isNaN(number)) number = 0;
                if (withCommas == null) withCommas = false;
                if (digits == 0) digits = 1;

                var integerPart = (decimalPlaces > 0 ? Math.floor(number) : Math.round(number));
                var string = "";

                for (var j = 0; j < digits || integerPart > 0; ++j) {
                    // Insert a comma every three digits.
                    if (withCommas && string.match(/^\d\d\d/))
                        string = "," + string;

                    string = (integerPart % 10) + string;
                    integerPart = Math.floor(integerPart / 10);
                }

                if (decimalPlaces > 0) {
                    number -= Math.floor(number);
                    number *= Math.pow(10, decimalPlaces);

                    string += "." + kb.util.text.formatNumber(number, decimalPlaces, 0);
                }

                return negative ? "-" + string : string;
            }
        },

        math: {
            toDecimal: function (number, minZero) {
                number = number ? number.toString() : '0';
                minZero = typeof minZero !== 'undefined' ? minZero : true;

                var simpleNumber = '';
                var negative = false;

                // Strips out characters that aren't a number or decimal.
                for (var i = 0; i < number.length; ++i) {
                    if ("0123456789.".indexOf(number.charAt(i)) >= 0)
                        simpleNumber += number.charAt(i);
                    if ("-".indexOf(number.charAt(i)) >= 0)
                        negative = true;
                }

                simpleNumber = parseFloat(simpleNumber);
                simpleNumber = negative ? simpleNumber * -1 : simpleNumber;

                if (minZero && simpleNumber < 0)
                    simpleNumber = 0.00;

                return simpleNumber;
            }
        }
    }
};





//Media Query & Media Type Matching
var mediaQuery = '';
$(function () {
    enquire.register("screen and (max-width:40em)", {
        match: function () {
            mediaQuery = 'SML';
            $('body').removeClass(function (index, css) { return (css.match(/(^|\s)viewport-\S+/g) || []).join(' '); }).addClass('viewport-small');
        }
    });
    enquire.register("screen and (min-width: 40.063em) and (max-width: 64em)", {
        match: function () {
            mediaQuery = 'MED';
            $('body').removeClass(function (index, css) { return (css.match(/(^|\s)viewport-\S+/g) || []).join(' '); }).addClass('viewport-medium');
        }
    });
    enquire.register("screen and (min-width: 40.063em) and (max-width: 60em)", {
        match: function () {
            //console.info('MQ+','MD1');
        }
    });
    enquire.register("screen and (min-width: 40.063em) and (max-width: 52.5em)", {
        match: function () {
            //console.info('MQ+','MD2');
        }
    });
    enquire.register("screen and (min-width: 40.063em) and (max-width: 48em)", {
        match: function () {
            //console.info('MQ+','MD3');
        }
    });
    enquire.register("screen and (min-width: 64.063em)", {
        match: function () {
            mediaQuery = 'LRG';
            $('body').removeClass(function (index, css) { return (css.match(/(^|\s)viewport-\S+/g) || []).join(' '); }).addClass('viewport-large');
        }
    });
    enquire.register("screen and (orientation: landscape)", {
        match: function () {
            $('body').removeClass(function (index, css) { return (css.match(/(^|\s)orientation-\S+/g) || []).join(' '); }).addClass('orientation-landscape');
        }
    });
    enquire.register("screen and (orientation: portrait)", {
        match: function () {
            $('body').removeClass(function (index, css) { return (css.match(/(^|\s)orientation-\S+/g) || []).join(' '); }).addClass('orientation-portrait');
        }
    });
    enquire.register("print", {
        match: function () {
            //console.info('MQ+','print');
        }
    });
});









// Get URL Parameters
// (returns true if param is present & converts string T/F to boolean T/F)
var qString = (function (a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=', 2);
        if (p.length == 1) {
            b[p[0]] = true;
        } else {
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
            if (b[p[0]].toLowerCase().trim() == 'true' || b[p[0]].toLowerCase().trim() == 'false') {
                b[p[0]] = Boolean(b[p[0]])
            }
        }
    }
    return b;
})(window.location.search.substr(1).split('&'));

$(document).ready(function () {


    //MyKB Broker Switch
    if (qString['login'] && qString['broker']) {
        $('.broker-toggle').toggles({ on: true });
        $('#IsBroker').val('true');
        $('#reg-label').html('Create a <span class="pre">MyKB</span> Broker Profile');
    }


    //Show Map View for Mobile
    if (qString['map'] && $('body.region-page').length && mediaQuery == 'SML') {
        setTimeout(function () {
            $('#map-view-button .map-view').trigger('click');
        }, 0);
    }


    //Email Link Redirect for Tracking Params
    if (qString['mailto']) {
        var mSender = 'mailto:' + qString['mailto'];
        if (qString['subject']) {
            //var mSubject = encodeURIComponent(qString.subject);
            var mSubject = qString['subject'];
            mSender += '?subject=' + mSubject;
        }
        if (qString['body']) {
            //var mSubject = encodeURIComponent(qString.subject);
            var mBody = qString['body'];
            mSender += '?body=' + mBody;
        }
        window.location.href = mSender;
    }


});










//Transform DSS Preview Paths
function transformPaths(styleSheetPath, scriptID, legacy) {
    var legacy = typeof legacy !== 'undefined' ? legacy : false;

    var pdPath = '/kb-assets/';

    if (legacy) { pdPath = '/content/'; }

    var pvPath = '/DSSPreview' + pdPath;
    var regex = new RegExp(pdPath, "g");

    //Workaround for DEV environment folder structure	
    if (window.location.href.indexOf('ingeniuxondemand') == -1) {
        pdPath = '/kbhomecms' + pdPath;
        pvPath = '/kbhomecms' + pvPath;
    }

    $.ajax({
        url: styleSheetPath,
        type: 'get',
        success: function (data) {

            $(scriptID).replaceWith('<style>' + data.replace(regex, pvPath) + '</style>');
            console.log('DSS PREVIEW TRANSFORM STYLESHEET SUCCESS');

        }
    });

}





// Page backgrounds from cms

function initCmsPageBg(image) {
    //TODO: use app url here
    imagePath = App.Url("/assets/images/" + image);
    $(".bg-CmsPage").css("background-image", "url(" + imagePath + ")");
}







// Page scripts
$(function () {

    $("#community-sort").change(function () {
        $("option:selected", $(this)).each(function () {
            var sortValue = $(this).val();
            SortRegion(sortValue);
            return false;
        });
    });

    $("#floorplan-sort").change(function () {
        $("option:selected", $(this)).each(function () {
            var sortValue = $(this).val();
            SortFloorPlan(sortValue);
            return false;
        });
    });

    $("#qmi-local-sort").change(function () {
        $("option:selected", $(this)).each(function () {
            var sortValue = $(this).val();
            SortQMILocal(sortValue);
            return false;
        });
    });

});









//Correct Dollar Notation (Used by Mortgage Calculator & Energy Cost Comparison Chart)
function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



//QMI Warranty Disclaimer (per Legal)
function addLegal() {
    var rVal = " (see KB Home Limited Warranty for full details.)";
    var wArray = {
        "Builder warranty": "Builder warranty" + rVal,
        "10-Year Limited Warranty": "10-Year Limited Warranty" + rVal,
        "Builder's limited warranty": "Builder's limited warranty" + rVal,
        "10-yr. warranty": "10-yr. warranty" + rVal,
        "10 year limited warranty": "10 year limited warranty" + rVal,
        "10-Year Warranty": "10-Year Warranty" + rVal,
        "10-year KB Home Limited Warranty": "10-Year Limited Warranty" + rVal,
        "new home warranty": "10-Year Limited Warranty" + rVal
    };
    $('.qmi-row .description').each(function () {
        var dText = $(this).html();
        for (var key in wArray) {
            dText = dText.replace(key, wArray[key]);
        }
        $(this).html(dText);
    });
}


//Completely Remove Hash from URL
function removeHash() {
    history.pushState("", document.title, window.location.pathname + window.location.search);
}


//Open Mobile Nav Tab
function openNavTab_m(id) {
    console.info('fn: openNavTab_m');
    $('.tab-pane, .accordion-navigation').removeClass('active');
    $(id).parent().addClass('active');
    $(id).addClass('active');
}


//Toggle Mobile Go to Top Button
function toggleNavTop() {
    var wHeight = parseInt($(window).height() + 200);
    var cHeight = parseInt($(document).height());
    if (cHeight > wHeight) {
        $('#navTop').show();
    } else {
        $('#navTop').hide();
    }
}



//Detect Printing & call printView function if available
(function () {
    var beforePrint = function () {
        console.info('Before Print');
        try {
            printView(true);
        } catch (err) { }
    };
    var afterPrint = function () {
        console.info('After Print');
        try {
            printView(false);
        } catch (err) { }
    };
    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function (mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;
}());





//Default Image Handling
function imgError(element, image) {
    var image = typeof image !== 'undefined' ? image : false;
    element.onerror = '';
    var dImg = {
        lg: 'assets/images/Coming%20Soon%20Images/photo-coming-soon-1200.jpg',
        md: 'assets/images/Coming%20Soon%20Images/photo-coming-soon-420.jpg',
        sm: 'assets/images/Coming%20Soon%20Images/photo-coming-soon.jpg'
    };
    var presets = {
        jumbo: '1600',
        xlarge: '1200',
        large: '1024',
        medium: '640',
        small: '420',
        xsmall: '180',
        tiny: '100'
    };
    if (image) {
        element.src = image;
    } else {
        var params = {}; element.src.replace(/[?&;]+([^=]+)=([^&;]*)/gi, function (str, key, value) { params[key] = value; });
        var size;
        if (params['preset'] && params['preset'] != 'convert-to-jpg') {
            size = presets[params['preset']];
        } else {
            size = params['width'];
        }
        if (parseInt(size) < 350) {
            element.src = dImg.sm;
        } else if (parseInt(size) < 800) {
            element.src = dImg.md;
        } else {
            element.src = dImg.lg;
        }
    }
    if ($('source', $(element).parent()).length) {
        $('source', $(element).parent()).each(function (index) {
            var srcset = $(this).attr('srcset');
            var srcsplit = srcset.split('?');
            $(this).attr('srcset', element.src + '?' + srcsplit[1]);
        });
    }
    return true;
}








$(function () {


    /*Global JQuery Functions*/

    //--Functions


    //Set iPad inital zoom
    var zoom = document.documentElement.clientWidth / window.innerWidth;


    function zPinch() {
        $(".lockfixed").addClass('zoomed');
        //iPad zoom detection
        var zoomNew = document.documentElement.clientWidth / window.innerWidth;
        if (zoomNew <= zoom) {
            $(".lockfixed").removeClass('zoomed');
        }
    }

    $(document).bind('pinch', zPinch);



    //--Window Resize
    window.addEventListener('resize', function () {


        //--LockFixed Adjustments - set the width of the div when window resizes

        //fluid box width
        if ($(window).width() > 640) {
            var $box = $('.lockfixed');
            $box.width($box.parent().outerWidth());
        }

        //window measurements
        var bHeight = $('.lockfixed .inner').outerHeight();
        var wHeight = $(window).height();

        if ($(window).width() < 641 || wHeight <= bHeight) { /*Window Width = 641*/
            $(".lockfixed").addClass('static');
        } else {
            $(".lockfixed").removeClass('static');
        }




    });

    //--DOM Ready
    $(document).ready(function () {

        $('#navTop').click(function () {
            $('html, body').animate({
                scrollTop: 0,
                scrollLeft: 0
            }, 500);
        });


        //--Correct iOS mis-identifying tel links
        $('.sqft a').contents().unwrap();


        //--Correct Initial iPad Zoom
        if (zoom > 1) {
            $(".lockfixed").addClass('zoomed');
        } else {
            $(".lockfixed").removeClass('zoomed');
        }


        //--Prevent iOS form Zooming
        var $viewportMeta = $('meta[name="viewport"]');
        $('input, select, textarea').bind('focus blur', function (event) {
            $viewportMeta.attr('content', 'width=device-width,initial-scale=1,maximum-scale=' + (event.type == 'blur' ? 10 : 1));
        });


        //--Browser Compatibility Check
        options = {
            // Specifies which browsers/versions will be blocked
            reject: {
                all: false, // Covers Everything (Nothing blocked)
                msie: 9 // Covers MSIE <= 6 (Blocked by default)
                /*
                * Many possible combinations.
                * You can specify browser (msie, chrome, firefox)
                * You can specify rendering engine (geko, trident)
                * You can specify OS (Win, Mac, Linux, Solaris, iPhone, iPad)
                *
                * You can specify versions of each.
                * Examples: msie9: true, firefox8: true,
                *
                * You can specify the highest number to reject.
                * Example: msie: 9 (9 and lower are rejected.
                *
                * There is also "unknown" that covers what isn't detected
                * Example: unknown: true
                */
            },
            display: [], // What browsers to display and their order (default set below)
            browserShow: true, // Should the browser options be shown?
            browserInfo: { // Settings for which browsers to display
                chrome: {
                    // Text below the icon
                    text: 'Google Chrome',
                    // URL For icon/text link
                    url: 'http://www.google.com/chrome/',
                    // (Optional) Use "allow" to customized when to show this option
                    // Example: to show chrome only for IE users
                    // allow: { all: false, msie: true }
                },
                firefox: {
                    text: 'Mozilla Firefox',
                    url: 'http://www.mozilla.com/firefox/'
                },
                safari: {
                    text: 'Safari',
                    url: 'http://www.apple.com/safari/download/'
                },
                opera: {
                    text: 'Opera',
                    url: 'http://www.opera.com/download/'
                },
                msie: {
                    text: 'Internet Explorer',
                    url: 'http://www.microsoft.com/windows/Internet-explorer/'
                }
            },

            // Pop-up Window Text
            header: 'Did you know that your Internet Browser is out of date?',

            paragraph1: 'Your browser is out of date, and may not display all features of this and other websites.',

            paragraph2: 'Click on an icon to download the latest browser version.',

            // Allow closing of window
            close: true,

            // Message displayed below closing link
            closeMessage: 'Your experience on this website may be degraded if you continue.',
            closeLink: 'Close This Window',
            closeURL: '#',

            // Allows closing of window with esc key
            closeESC: true,

            // Use cookies to remmember if window was closed previously?
            closeCookie: true,
            // Cookie settings are only used if closeCookie is true
            cookieSettings: {
                // Path for the cookie to be saved on
                // Should be root domain in most cases
                path: '/',
                // Expiration Date (in seconds)
                // 0 (default) means it ends with the current session
                expires: 0
            },

            // Path where images are located
            imagePath: '/content/images/',
            // Background color for overlay
            overlayBgColor: '#000',
            // Background transparency (0-1)
            overlayOpacity: 0.8,

            // Fade in time on open ('slow','medium','fast' or integer in ms)
            fadeInTime: 'fast',
            // Fade out time on close ('slow','medium','fast' or integer in ms)
            fadeOutTime: 'fast',

            // Google Analytics Link Tracking (Optional)
            // Set to true to enable
            // Note: Analytics tracking code must be added separately
            analytics: false
        };
        $.reject(options);







    });












    ////
});