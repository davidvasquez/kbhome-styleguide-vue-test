

$(function () {
	MyKB.init();
});

var postLoginURL = "";

var MyKB = {

    optisRegistered: false,

    init: function () {
        /*to call reveal modal box 
        Use following on <a href="">
		 
        <a href="" data-reveal-id="login-dialog">
		 
        to call via code 
		 
        $('#login-dialog').foundation('reveal', 'open');
		 
        * */


        $.validator.unobtrusive.adapters.addBool("mandatory", "required");




        $("#create-account").click(function () {
            $('#login-dialog .close-reveal-modal').trigger('click.modalEvent');

            MyKB.populateRegistrationDropDowns();

            MyKB.afterLogin = function () {
                $('#registered-confirm-dialog #confirm-message').html("You can now save communities and floor plans to your MyKB profile.");
                $('#registered-confirm-dialog').foundation('reveal', 'open');
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-New' });
                console.info('GTM-MKB001 event: pushedVPV, virtualPageview: /MyKb-New');
            };
        });

        $("#have-account").click(function () {
            $('#register-dialog .close-reveal-modal').trigger('click.modalEvent');
            if ($.isFunction(MyKB.afterLogin)) {
                var cached_function = MyKB.afterLogin;
                MyKB.afterLogin = function () {                    
                    cached_function.apply(this);
                };
            } else {
                MyKB.afterLogin = function () {
                    $(this).closest(".reveal-modal").find(".close-reveal-modal").trigger('click.modalEvent');
                    location.href = App.Url("/My-KB");
                };
            }            
            dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Return' });
            console.info('GTM-MKB002 event: pushedVPV, virtualPageview: /MyKb-Return');          
        });

        $("#create-broker-account").click(function () {
            $("#IsBroker").val("true");
            $("#switch-account-type").text("Home Buyers Sign Up Here");
            $('#login-dialog .close-reveal-modal').trigger('click.modalEvent');

            MyKB.populateRegistrationDropDowns();

            MyKB.afterLogin = function () {
                $('#registered-confirm-dialog #confirm-message').html("You can now save communities and floor plans to your MyKB profile.");
                $('#registered-confirm-dialog').foundation('reveal', 'open');
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-New' });
                console.info('GTM-MKB003 event: pushedVPV, virtualPageview: /MyKb-New');
            };
        });



		//--Toggle: UI Element
		// With options (defaults shown below)
		$('.broker-toggle').toggles({
		    drag: true, // can the toggle be dragged
		    click: true, // can it be clicked to toggle
		    text: {
		      on: 'YES', // text for the ON position
		      off: 'NO' // and off
		    },
		    on: false, // is the toggle ON on init
		    animate: 250, // animation time
		    transition: 'ease-in-out', // animation transition,
		    checkbox: null, // the checkbox to toggle (for use in forms)
		    clicker: null, // element that can be clicked on to toggle. removes binding from the toggle itself (use nesting)
		    width: 60, // width used if not set in css
		    height: 25, // height if not set in css
		    type: 'compact' // if this is set to 'select' then the select style toggle will be used
		});
		// Getting notified of changes, and the new state:
		$('.broker-toggle').on('toggle', function (e, active) {
		    if (active) {
		        $('input#IsBroker').val('true');
		        $('#reg-title').addClass('broker');
		        $('#reg-title h3').html('Create a <span class="pre">MyKB</span> Broker Profile');
	            dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'KB Broker', 'eventAction': 'Click', 'eventLabel': 'BrokersSignUpHere:' + window.location.pathname });
	            console.info('GTM-MKB004 event: pushedEvent, eventAction: Click, eventCategory: KB Broker, eventLabel: BrokersSignUpHere:' + window.location.pathname);
		    } else {
		        $('input#IsBroker').val('false');
		        $('#reg-title').removeClass('broker');
		        $('#reg-title h3').html('Create a <span class="pre">MyKB</span> Profile');
	            dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'My KB', 'eventAction': 'Click', 'eventLabel': 'HomeBuyersSignUpHere:' + window.location.pathname });
	            console.info('GTM-MKB005 event: pushedEvent, eventAction: Click, eventCategory: My KB, eventLabel: HomeBuyersSignUpHere:' + window.location.pathname);
		    }
		});




        $("#broker-create").click(function (e) {
            if ($("#mykb").attr("data-logged-in") == "True") {
                location.href = App.Url("/My-KB/");
                return;
            }	        
            $('.broker-toggle').toggles({on: true});			        
            $("#IsBroker").val("true");
            $('#reg-title').addClass('broker');
	        $('#reg-title h3').html('Create a <span class="pre">MyKB</span> Broker Profile');
            $('#register-dialog').foundation('reveal', 'open');
            e.preventDefault();
            dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Create' });
            console.info('GTM-MKB006 event: pushedVPV, virtualPageview: /MyKb-Create');
        });

        $("#homebuyer-create").click(function (e) {
            if ($("#mykb").attr("data-logged-in") == "True") {
                location.href = App.Url("/My-KB/");
                return;
            }
            $('.broker-toggle').toggles({ on: false });
            $('#reg-title h3').html('Create a <span class="pre">MyKB</span> Profile');
            $('#register-dialog').foundation('reveal', 'open');
            e.preventDefault();
            dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Create' });
            console.info('GTM-MKB007 event: pushedVPV, virtualPageview: /MyKb-Create');
        });




        //BEGIN FACEBOOK LOGIN/REGISTER ///////////
        //var FB_app_id = "239739552770716"; //production
        //var FB_app_id = "285358881518854"; //staging
        var FB_redir_url = $("base").attr("href");

        $("#facebook-login").click(function () {
            console.log('ID: ' + FB_APP_ID);
            $('#login-dialog .close-reveal-modal').trigger('click.modalEvent');
            var url = "https://www.facebook.com/dialog/oauth/?scope=email&client_id=" + FB_APP_ID + "&redirect_uri=" + FB_redir_url + "My-KB/FacebookLogin";
            var windowSize = "width=1024,height=525,scrollbars=no";
            window.open(url, 'popup', windowSize);
            return false;
        });

        $("#facebook-register").click(function () {
            $('#login-dialog .close-reveal-modal').trigger('click.modalEvent');
            var url = "https://www.facebook.com/dialog/oauth/?scope=email&client_id=" + FB_APP_ID + "&redirect_uri=" + FB_redir_url + "My-KB/FacebookLogin";
            var windowSize = "width=1024,height=525,scrollbars=no";
            window.open(url, 'popup', windowSize);
            return false;
        });
        


        // forgot password
        //
        $(".forgot-password-link").click(function () {
            $('#login-dialog .close-reveal-modal').trigger('click.modalEvent');
            $('#forgot-password-dialog').foundation('reveal', 'open');
            return false;
        });


        $(".close-button").click(function () {
            $(this).closest(".reveal-modal").find(".close-reveal-modal").trigger('click.modalEvent');
        });

        $(".mykb-button").click(function () {
            $(this).closest(".reveal-modal").find(".close-reveal-modal").trigger('click.modalEvent');
            location.href = App.Url("/My-KB");
        });





        $("#mykb, .mykb.mtab").click(function (e) {
            if (!($(this).hasClass("logout"))) {
                e.preventDefault();
                if ($(this).attr("data-login-url")) {
                    location.href = $(this).attr("data-login-url");
                    return false;
                }

                if ($(this).attr("data-logged-in") === "True") {
                    location.href = App.Url("/My-KB/");
                    return false;
                }

                var registered = $.cookie('ExistingUser');
                if (registered != null) {
                    $('#login-dialog').foundation('reveal', 'open');
                    dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Login' });
                    console.info('GTM-MKB007 event: pushedVPV, virtualPageview: /MyKb-Login');
                    return false;
                } else {
                    $('#register-dialog').foundation('reveal', 'open');
                    MyKB.populateRegistrationDropDowns();
                    dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Create' });
                    console.info('GTM-MKB008 event: pushedVPV, virtualPageview: /MyKb-Create');
                    return false;
                }

                $('#login-dialog').foundation('reveal', 'open');
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Login' });
                console.info('GTM-MKB009 event: pushedVPV, virtualPageview: /MyKb-Login');
                MyKB.afterLogin = function () { location.href = App.Url("/My-KB/"); };
                return false;
            }
            else {
                location.href = App.Url("/My-KB/Logout");
            }
        });



        $("#save-community").change(function() {
            var id = $(this).val();
            if ($("#mykb").attr("data-logged-in") == "True") {
                try {
                    ewt.track({ name: communityName, type: 'savefloorplan' });
                    console.info('GTM-MKB010 ewt.track - name: ' + communityName + ', type: savefloorplan');
                } catch (err) {}
                MyKB.saveCommunity(id);
                return false;
            }

            // not logged in, set the afterLogin callback and prompt the user
            // to log in.
            MyKB.afterLogin = function () {
                try {
                    ewt.track({ name: communityName, type: 'savefloorplan' });
                    console.info('GTM-MKB011 ewt.track - name: ' + communityName + ', type: savefloorplan');
                } catch (err) {}
                MyKB.saveCommunity(id);
            };

            var registered = $.cookie('ExistingUser');
            if (registered != null) {
                $('#login-dialog').foundation('reveal', 'open');
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Login' });
                console.info('GTM-MKB012 event: pushedVPV, virtualPageview: /MyKb-Login');
                return false;
            } else {
                $('#register-dialog').foundation('reveal', 'open');
                MyKB.populateRegistrationDropDowns();
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Create' });
                console.info('GTM-MKB013 event: pushedVPV, virtualPageview: /MyKb-Create');
                return false;
            }
        });




		//Community & Floor Plan Page - Compare Floor Plans
        //$("#save-floor-plan, .compareFP-btn").click(function(){
        $("#save-floor-plan").on('click', function () {

            var id = $(this).data("floorplanid");
            var numItems = parseInt($("#compare-bar").data('saved-plans'));

			if($(this).hasClass("active")){
                //Deactivate
                numItems--;
                if($("#mykb").attr("data-logged-in") == "True"){
                    $.post(App.Url("/My-KB/DeleteFloorPlan"), { floorPlanId: id });
		            }else{
		            $.post(App.Url("/My-KB/TempDeleteFloorPlan"), { planID: id });
		            }               
                $(this).removeClass("active");
                if(numItems <= 0){$("#compare-infobar, #sortbar").removeClass("active");}
            }else{
                //Activate
                numItems++;
                if($("#mykb").attr("data-logged-in") == "True"){
                    $.post(App.Url("/My-KB/AddFloorPlan"), { floorPlanId: id });
	                }else{
	                $.post(App.Url("/My-KB/TempSaveFloorPlan"), { planID: id });
	                }
                $(this).addClass("active");
                $("#compare-infobar, #sortbar").addClass("active");
            }
			$("#compare-bar").data('saved-plans',numItems);
			if(numItems > 0){
            	$(".fp-counter").text(numItems);
            }else{
	            $(".fp-counter").text('no');
            }
			$(".fp-ordinal").text(((numItems == 1) ? '' : 's'));
			
			
			console.log('f '+numItems);
			
			          
        });




		//Login to MyKB & Compare
        $(".compare-button").click(function () {
            if ($("#mykb").attr("data-logged-in") == "True") {
                location.href = App.Url("/My-KB#floorplans");
                return false;
            }
            // not logged in, set the afterLogin callback and prompt the user to log in.
            MyKB.afterLogin = function () {
                MyKB.saveFloorPlans();
                postLoginURL = ("/My-KB#floorplans");
            };
            var registered = $.cookie('ExistingUser');
            if (registered != null) {
                $('#login-dialog').foundation('reveal', 'open');
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Login' });
                console.info('GTM-MKB034 event: pushedVPV, virtualPageview: /MyKb-Login');
                return false;
            } else {
                $('#register-dialog').foundation('reveal', 'open');
                MyKB.populateRegistrationDropDowns();
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Create' });
                console.info('GTM-MKB035 event: pushedVPV, virtualPageview: /MyKb-Create');
                return false;
            }
        });




        $("#updates").click(function () {
            if ($("#mykb").attr("data-logged-in") == "True") {
                $("#community-updates-dialog").foundation('reveal', 'open');
                return false;
            }

            // not logged in..
            MyKB.afterLogin = function (isNewReg) {
                if (isNewReg) {
                    $("#community-updates-dialog h2").text("Thank you for Registering");
                }

                $("#community-updates-dialog").foundation('reveal', 'open');
            };

            var registered = $.cookie('ExistingUser');
            if (registered != null) {
                $('#login-dialog').foundation('reveal', 'open');
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Login' });
                console.info('GTM-MKB018 event: pushedVPV, virtualPageview: /MyKb-Login');
                return false;
            } else {
                $('#register-dialog').foundation('reveal', 'open');
                MyKB.populateRegistrationDropDowns();
                dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Create' });
                console.info('GTM-MKB019 event: pushedVPV, virtualPageview: /MyKb-Create');
                return false;
            }
        });


		/* -- DEPRICATED 06/2016
        $("a.map").click(function () {
            openDrivingDirectionsMap("", $(this).attr("data-end-address"));
            return false;
        });
		*/

        $("#alerts form").submit(function () {
            if ($(this).valid()) {
                $.ajax({
                    url: this.action,
                    type: this.method,
                    data: $(this).serialize(),
                    success: function (result) {
                        $('#alerts form .submit').val("Saved!");
                    }
                });
            }
            return false;
        });

        $("#alerts :password").change(function () {
            $("#PasswordChanged").val("true");
        });

        $("#alerts :input").change(function () {
            $("#alerts form .submit").val("Save");
        });

        $(".community-subscribe-cb").change(function () {
            var el = $(this);

            if ($(this).is(":checked")) {
                MyKB.addCommunitySub($(this).attr("data-id"), new function () {
                    el.closest(".community-listing")
						.find(".info")
						.clone()
						.append("<a href='#' class='remove-region' data-id='" + el.attr("data-id") + "'>Remove</a>")
						.appendTo(".updates")
						.wrap("<div class='update-cell' />");
                });
            }
            else {
                MyKB.deleteCommunitySub($(this).attr("data-id"), new function () {
                    $(".updates [data-id='" + el.attr("data-id") + "']").closest(".update-cell").remove();
                });
            }
        });
        $("#recentcommunities .remove-saved-community").click(function () {
            var ele = $(this);
            $.post(App.Url("/My-KB/DeleteRecentCommunity"), { communityId: $(this).attr("data-id") }, function () {
                ele.closest(".community-listing").hide('slow');
            });

            return false;
        });
        $("#savedcommunities .remove-saved-community").click(function () {
            var ele = $(this);
            $.post(App.Url("/My-KB/DeleteCommunity"), { communityId: $(this).attr("data-id") }, function () {
                ele.closest(".community-listing").hide('slow');
            });

            return false;
        });
        $("#OffersByPhone").click(function () {
            var ele = $(this);
            var phone = $("#Phone").val();

            if (phone == "") {
                ele.removeAttr('checked');
                $("#phonechkerror").show();
            }
            else {
                $("#phonechkerror").hide();
            }
        });

        $(".remove-saved-floor-plan").click(function () {
            var ele = $(this);
            $.post(App.Url("/My-KB/DeleteFloorPlan"), { floorPlanId: $(this).attr("data-id") }, function () {


			if($(ele).parent().hasClass('active') && $('.compare-panel').length > 1){
				$('#compare-slider .compare-panel:first-child .click-to-pin').trigger("click");
			}

        	$(ele).parent().remove();
        	
        	if($('.compare-panel').length == 0){
				$('#compare-viewer, .compare-info').removeClass('yes-fp').addClass('no-fp');
			}else if($('.compare-panel').length == 2 || $('.compare-panel').length == 1){
				$('#compare-add-msg').show();
			}
			
			
			if($('#compare-viewer').hasClass('compare-panes-2')){
				$('#compare-viewer').removeClass('compare-panes-2').addClass('compare-panes-1');
			}	
			if($('#compare-viewer').hasClass('compare-panes-3')){
				$('#compare-viewer').removeClass('compare-panes-3').addClass('compare-panes-2');
			}
			if($('#compare-viewer').hasClass('compare-panes-4')){
				$('#compare-viewer').removeClass('compare-panes-4').addClass('compare-panes-3');
			}
			if($('#compare-viewer').hasClass('compare-panes-5')){
				$('#compare-viewer').removeClass('compare-panes-5').addClass('compare-panes-4');
			}
			if($('#compare-viewer').hasClass('compare-panes-6') && $('.compare-panel').length == 5){
				$('#compare-viewer').removeClass('compare-panes-6').addClass('compare-panes-5');
			}			
			




            });

            return false;
        });

        $(".remove-community").click(function () {
            var ele = $(this);
            $.post(App.Url("/My-KB/DeleteCommunitySubscription"), { communityId: $(this).attr("data-id") }, function () {
                ele.closest(".update-cell").hide('slow');
            });

            return false;
        });

        $(".remove-region").click(function () {
            var ele = $(this);
            $.post(App.Url("/My-KB/DeleteRegionSubscription"), { communityId: $(this).attr("data-id") }, function () {
                ele.closest(".update-cell").hide('slow');
            });

            return false;
        });


        $("#Password").focus(
            function () {
                var pass = $('<input class="field-1" data-val="true" data-val-required="The Password field is required." id="Password" name="Password" placeholder="Password" type="Password" />');
                $(this).replaceWith(pass);
                pass.focus();
            }
        );

        $("#RegPassword").focus(
            function () {
                var pass = $('<input class="field-1" data-val="true" data-val-required="The Password field is required." id="RegPassword" name="Password" placeholder="Password" type="Password" />');
                $(this).replaceWith(pass);
                pass.focus();
            }
        );

        // show mykb login if ?login is in the query string
        if (location.href.indexOf('?login') > 0 || location.href.indexOf("&login") > 0) {
            $("#mykb").click();
            //console.log("currentRegion",currentRegion);
            //console.log("currentCommunity",currentCommunity);
        }

    },

    addCommunitySub: function (id, callback) {
        $.post(App.Url("/My-KB/AddCommunitySubscription"), { communityId: id }, callback);
    },

    deleteCommunitySub: function (id, callback) {
        $.post(App.Url("/My-KB/DeleteCommunitySubscription"), { communityId: id }, callback);
    },

    showValidationErrors: function (el, errors) {
        el.empty();
        $.each(errors, function (i, error) {
            if (error[1] != "")
                el.append("<li>" + error + "</li>");
        });
    },

    saveCommunity: function (id) {
        var url = $("#save-community").is(":checked") ? "/My-KB/AddCommunity" : "/My-KB/DeleteCommunity";

        $.post(App.Url(url), { communityId: id }, function (data) {
            $("#save-community").prop("checked", data.IsSaved);
            if(data.IsSaved){
	            $('label[for="save-community"]').text("Community Saved");
	            $('#save-community-btn i').attr("class","icon-star");
			} else {
				$('label[for="save-community"]').text("Save this Community");
				$('#save-community-btn i').attr("class","icon-star-empty");
			}        
        });
    },

    saveFloorPlan: function (id) {
        if ($("#mykb").attr("data-logged-in") == "True") {
            var url = $("#save-floor-plan").is(":checked") ? "/My-KB/AddFloorPlan" : "/My-KB/DeleteFloorPlan";
            $.post(App.Url(url), { floorPlanId: id }, function (data) {
                $("#save-floor-plan").prop("checked", data.IsSaved);
                $("label[for=save-floor-plan]").text(data.IsSaved ? "Saved" : "Save Floor Plan");
            });
        }
        else {
            var url = $("#save-floor-plan").is(":checked") ? "/My-KB/TempSaveFloorPlan" : "/My-KB/TempDeleteFloorPlan";
            $.post(App.Url(url), { planId: id }, function (data) {
                $("#save-floor-plan").prop("checked", data.IsSaved);
                $("label[for=save-floor-plan]").text(data.IsSaved ? "Saved" : "Save Floor Plan");
            });
        }
    },

    saveFloorPlans: function() {
        var url = "/My-KB/AddFloorPlans";
        var executeURL = App.Url(url);
        $.post(App.Url(url), function () {
            location.href = App.Url("/My-KB#floorplans");
        });
        console.info('Posted To Account');
    },

    populateRegistrationDropDowns: function () {
        if (typeof currentRegion === 'undefined') {
            return;
        }

        if (currentRegion != "") {
            $('#RegionList').val(currentRegion);
            $("#RegionList").val(currentRegion).change();
            initialRegion = "";
            }

        

    }

};

// gets the specific key value from the cookie object
function getCookie(sender, key) {
    var ca = sender.split('&');
    var name = key + "=";

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return null;
}

// called when we get status: 200 OK from 
// a login call
function loginSuccess(response) {
	
    //--Check if Login is Successful or Failed
    if (!response.IsValid) {
		MyKB.showValidationErrors($("#login-form .validation-summary"), response.Errors);
		return;
	}

    //--Check if Cookies are Enabled
	var cookieEnabled = (navigator.cookieEnabled) ? true : false;
	if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
	    document.cookie = "testcookie";
	    cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
	}
	if (!cookieEnabled) {
	    $("#login-form .validation-summary").append("<li>Cookies must be enabled to log in.</li>");
	    return;
	}

    //--Login Success
	var registered = $.cookie("ExistingUser");
	if (registered) {
	    var userid = getCookie(registered, "UserId");
	    if (userid) {
	        dataLayer.push({ 'event': 'set_userid', 'UserId': userid });
	        console.info('event: set_userid, UserId: ' + userid);
        }
	}

	$('#login-dialog .close-reveal-modal').trigger('click.modalEvent');
	$("#mykb").attr("data-logged-in", "True");
	dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-Return' });
	console.info('GTM-MKB020 event: pushedVPV, virtualPageview: /MyKb-Return');
	
	if ($.isFunction(MyKB.afterLogin)) {
		MyKB.afterLogin(false);
		MyKB.afterLogin = null;
	}
	if (postLoginURL != "") {
	    //location.href = App.Url(postLoginURL);
	    postLoginURL = "";
	} else {
	    location.href = App.Url("/My-KB");
	}
}



// called when we get status: 200 OK from 
// a registration 
function registrationSuccess(response) {
	
    //--Check if Login is Successful or Failed
    if (!response.IsValid) {
		MyKB.showValidationErrors($("#register-form .validation-summary"), response.Errors);
		return;
	}

	//--Registration Success
    var registered = $.cookie("ExistingUser");
    if (registered) {
        var userid = getCookie(registered, "UserId");
        if (userid) {
            dataLayer.push({ 'event': 'set_userid', 'UserId': userid });
            console.info('event: set_userid, UserId: ' + userid);
        }
    }

    $('#register-dialog .close-reveal-modal').trigger('click.modalEvent');
    $('#registered-confirm-dialog #confirm-message').html("You can now save communities and floor plans to your MyKB profile.");
    $('#registered-confirm-dialog').foundation('reveal', 'open');
    $("#mykb").attr("data-logged-in", "True");
    if ($("#IsBroker").val() == "true") {
        dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'KB Broker', 'eventAction': 'Click', 'eventLabel': 'BrokerRegister:my-kb-new:' + window.location.pathname });
        console.info('GTM-MKB021 event: pushedEvent, eventAction: Click, eventCategory: KB Broker, eventLabel: BrokerRegister:my-kb-new:' + window.location.pathname);
    } else {
        dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'My KB', 'eventAction': 'Click', 'eventLabel': 'Register:my-kb-new:' + window.location.pathname });
        console.info('GTM-MKB022 event: pushedEvent, eventAction: Click, eventCategory: My KB, eventLabel: Register:my-kb-new:' + window.location.pathname);
    }
    dataLayer.push({ 'event': 'pushedRegister' });
    console.info('GTM-MKB023 event: pushedRegister');
    dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/MyKb-New' });
    console.info('GTM-MKB024 event: pushedVPV, virtualPageview: /MyKb-New');

	if ($.isFunction(MyKB.afterLogin)) {
		MyKB.afterLogin(true);
		MyKB.afterLogin = null;
	}

}



function fbLoginSuccess(response) {
    //--Check if Login is Successful or Failed
    if (!response.IsValid) {
        MyKB.showValidationErrors($("#login_facebook .validation-summary"), response.Errors);
        return;
    }

    //--FB Login Success
    var registered = $.cookie("ExistingUser");
    if (registered) {
        var userid = getCookie(registered, "UserId");
        if (userid) {
            dataLayer.push({ 'event': 'set_userid', 'UserId': userid });
            console.info('event: set_userid, UserId: ' + userid);
        }
    }

    window.close();
}


function fbRegistrationSuccess(response) {
	//--Check if Login is Successful or Failed
    if (!response.IsValid) {
	    MyKB.showValidationErrors($("#register_facebook .validation-summary"), response.Errors);
		return;
	}

    dataLayer.push({ 'event': 'pushedFacebookRegister' });
    console.info('GTM-MKB025 event: pushedFacebookRegister');

    if ($("#IsBroker").val() == "true") {
	    dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'KB Broker', 'eventAction': 'Click', 'eventLabel': 'BrokerRegister with Facebook:/my-kb-new:' + window.location.pathname });
	    console.info('GTM-MKB026 event: pushedEvent, eventAction: Click, eventCategory: KB Broker, eventLabel: BrokerRegister with Facebook:/my-kb-new:' + window.location.pathname);
    } else {
	    dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'My KB', 'eventAction': 'Click', 'eventLabel': 'Register with Facebook:/my-kb-new:' + window.location.pathname });
	    console.info('GTM-MKB027 event: pushedEvent, eventAction: Click, eventCategory: My KB, eventLabel: BrokerRegister with Facebook:/my-kb-new:' + window.location.pathname);
    }

	// FB Registration Success
    var registered = $.cookie("ExistingUser");
    if (registered) {
        var userid = getCookie(registered, "UserId");
        if (userid) {
            dataLayer.push({ 'event': 'set_userid', 'UserId': userid });
            console.info('event: set_userid, UserId: ' + userid);
        }
    }
    //$("#confirm").show();
    //$("#registerform").hide();

    window.opener.loggedInViaFacebook("register");
    window.close();
}


function forgotPasswordSuccess(response) {
	if (!response.IsValid) {
		// login failed, hopefully with errors
		// display those errors in the validation summary
		MyKB.showValidationErrors($("#forgot-password-form .validation-summary"), response.Errors);
		return;
	}

	$("#forgot-password-form .prompt").hide();
	$("#forgot-password-form .success").show();
}

function subscribeSuccess(response) {
	$('#community-updates-dialog').foundation('reveal', 'close');
}


function ajaxFailure(response, status, data) {
    $(".validation-summary").removeClass("validation-summary-valid").addClass("validation-summary-errors");
	$(".validation-summary ul")
		.empty()
		.append("<li>A server error occurred. Please try again later.</li>");
}

function loggedInViaFacebook(action) {
	$("#mykb").attr("data-logged-in", "True");

	if ($.isFunction(MyKB.afterLogin)) {
		MyKB.afterLogin(false);
		MyKB.afterLogin = null;
    }

    //if (action != "register") {
    //    location.href = App.Url("/My-KB");
    //}

	location.href = App.Url("/My-KB");
}

//contact-cms-support

function ContactCMSSuccess(response) {
    if (!response.IsValid) {
        // login failed, hopefully with errors
        // display those errors in the validation summary
        MyKB.showValidationErrors($("#contactCMS-form .validation-summary ul"), response.Errors);
        return;
    }

    $("#contact-cms").hide();
    $("#contact-cms-response").show();
}

//contact-sales 

function ContactUsSuccess(response) {
	if (!response.IsValid) {
		// login failed, hopefully with errors
		// display those errors in the validation summary
		MyKB.showValidationErrors($("#contactUs-form .validation-summary ul"), response.Errors);
		return;
	}
	
	$("#contact-sales").hide();
	$("#contact-sales-response").show();
	dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'Body Link', 'eventAction': 'Click', 'eventLabel': 'Contact Us-Contact Sales:Submit' });
	console.info('GTM-MKB028 event: pushedEvent, eventAction: Click, eventCategory: Body Link, eventLabel: Contact Us-Contact Sales:Submit');
	dataLayer.push({ 'event': 'pushedContactSales' });
	console.info('GTM-MKB029 event: pushedContactSales');
	dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/contact-sales/thank-you' });
	console.info('GTM-MKB030 event: pushedVPV, virtualPageview: /contact-sales/thank-you');
}

//contact-warranty

function ContactWarrantySuccess(response) {
    if (!response.IsValid) {
        MyKB.showValidationErrors($("#contactWarranty-form .validation-summary ul"), response.Errors);
        return;
    }

    $("#contact-warranty").hide();
    $("#contact-warranty-response").show();
    dataLayer.push({ 'event': 'pushedEvent', 'eventCategory': 'Body Link', 'eventAction': 'Click', 'eventLabel': 'Contact Us-Warranty Services:Submit' });
    console.info('GTM-MKB031 event: pushedEvent, eventAction: Click, eventCategory: Body Link, eventLabel: Contact Us-Warranty Services:Submit');
    dataLayer.push({ 'event': 'pushedContactWarranty' });
    console.info('GTM-MKB032 event: pushedContactWarranty');
    dataLayer.push({ 'event': 'pushedVPV', 'virtualPageview': '/warranty-services/thank-you' });
    console.info('GTM-MKB033 event: pushedVPV, virtualPageview: /warranty-services/thank-you');
}

//community landing contact

function LandingContactUsSuccess(response) {
    if (!response.IsValid) {
        MyKB.showValidationErrors($("#landingcontactUs-form .validation-summary ul"), response.Errors);
        return;
    }

    $("#contact-form").hide();
    $("#contact-response").show();

}