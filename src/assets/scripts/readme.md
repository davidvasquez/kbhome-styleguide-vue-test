#Folder Explanation

-global
	All scripts that are globally included on the site and concatenated
	and compressed down into the global-scripts.js file are located here.

-modules
	All scripts that should NOT be included in the global-scripts.js file
	should be located here, even if it is intended to be included globally
	(but as a separate file).

-plugins
	Any external scripts that are written or maintained by a non-KB
	employee or source should be located under plugins and (preferrably)
	should include documentation about its usage within its respective
	folder.