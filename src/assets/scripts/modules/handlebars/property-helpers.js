Handlebars.registerHelper('pluralize', function(word, num){
	if(num != '1' || num != 1){
		var lastChar = word.substr(word.length - 1);
		if(lastChar == 'x'){
			return word + 'es';
		}else{
			return word + 's';
		}
	}else{
		return word;
	}
});

Handlebars.registerHelper('getBathroomsString', function(min, max){
    if(min == max){
        if(min == '1'){
            return min + " Bathroom";
        }else{
            return min + " Bathrooms";
        }
    }else{
        return min + " to " + max + " Bathrooms";
    }
});

Handlebars.registerHelper('getDashRange', function(min, max){
    if(min == max){
        return min;
    }else{
        return min + "- to " + max;
    }
});

Handlebars.registerHelper('phoneDigitsOnly', function(phoneIn) {
	phoneIn = typeof phoneIn !== 'undefined' ?  phoneIn : '';
    var output = phoneIn.replace(/\D/g, '');
    return new Handlebars.SafeString(output);
});

Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if (a == b) {
        return opts.fn(this);
    } else {
        return opts.inverse(this);
    }
});

Handlebars.registerHelper('if_not', function(a, b, opts) {
    if (a == b) {
		return opts.inverse(this);
    } else {
        return opts.fn(this);
    }
});