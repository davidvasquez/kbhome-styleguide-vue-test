/*================================================
=            Button Types and Styling            =
================================================*/

/// Lightness Tolerance:  This is a local variable used only in the buttons mixin.
/// Value from 0 - 100 which adjusts the tolerance of a button background's
/// lightness value. Lower values allow the button to switch over to a
/// white text color less easily.  Higher values reduce the likelihood of a
/// button's color to switch to white.
/// @example
/// 	@if (lightness( $color ) > 50 ) {
///			color: black;
/// 	} @else {
/// 		color: white;
/// 	}
$lightness-tolerance: 57;


/// Button Types:  Standard, Punchout, Punchout-Inverted, and
/// Punchout-Inverted-Masked. A mixin used to pass in a color
/// from the primary .button class and evaluate if the colors
/// from the text should be light or dark to remain legible
/// on the button's background color.  Also evaluates several
/// button styles as detailed above.
/// @example .class-name { @include button-color($palette-primary); }
/// @arg {string} $color - a color value to be used as the background color of the button.
@mixin button-color($color) {
	background-color: $color;
	@if (lightness($color) > $lightness-tolerance ) {
		color: $palette-dark;

		&::before,
		&::after { color: $palette-dark; }
	}
	@else {
		color: $palette-light;

		&::before,
		&::after { color: $palette-light; }
	}
	&:hover,
	&.hover {
		background-color: lighten($color, 10%);
		@if (lightness($color) > $lightness-tolerance ) {
			color: $palette-dark;
		}
		@else {
			color: $palette-light;
		}
	}

	&.punchout {
		transition: none;
		border: 1px solid $color; //lighten($color, 20%);
		background-color: transparent;
		color: $color;
		font-weight: 600;

		&:hover,
		&.hover {
			background-color: $color;
			color: $palette-light;
			box-shadow: none;
		}
		&.inverted {
			transition: box-shadow 0.25s ease-in-out;
			border: 1px solid $palette-light;
			background-color: transparent;
			color: $palette-light;

			&:hover,
			&.hover {
				transition: box-shadow 0.25s ease-in-out;
				box-shadow: inset 0 0 0 2px $palette-light;
				}

			&.masked {
				transition: background-color .15s ease-in-out, box-shadow 0.25s ease-in-out;
				background-color: rgba(darken($palette-dark-grey, 18%), .6);

				&:hover,
				&.hover { background-color: rgba(darken($palette-dark-grey, 10%), .75); }
			}
		}
	}
}

/// Gradient Buttons:  Works the same as button-color, but takes two arguments for creating
/// a three-dimensional effect with gradients and drop-shadows. It is possible and preferred
/// to utilize the same color value but with one being modified via sass color functions such
/// as lighten, darken, hue, etc.
/// @example Line Line 1 uses two colors. Line 2 uses one color modified via the sass lighten function.
///		.class-name { @include button-gradient($palette-light-grey, $palette-dark-grey); }
/// 	.class-name { @include button-gradient($palette-primary, lighten($palette-primary, 10%)); }
/// @arg {string} $color-primary - a color value to be used as the gradient start point.
/// @arg {string} $color-secondary - a color value to be used as the gradient end point.
@mixin button-gradient($color-primary, $color-secondary) {
	transition: .15s box-shadow ease-in-out;
	border: 1px solid rgba(0, 0, 0, 0.1);
	border-bottom: 1px solid rgba(0, 0, 0, 0.2);
	background: linear-gradient(to bottom, $color-primary 0%, $color-secondary 100%);
	background-color: $color-primary;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.3);

	@if (lightness($color-primary) > $lightness-tolerance ) {
		color: $palette-dark;
	}
	@else {
		color: $palette-light;
	}
	&:hover,
	&.hover {
		background: linear-gradient(to bottom, lighten($color-primary, 10%) 0%, lighten($color-secondary, 10%) 100%);
		background-color: lighten($color-primary, 10%);
		box-shadow: 0 1px 3px rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.15);

		@if (lightness($color-primary) > $lightness-tolerance ) {
			color: $palette-dark;
		}
		@else {
			color: $palette-light;
		}
	}
	&:active,
	&.active { background: linear-gradient(to bottom, $color-secondary 0%, $color-primary 100%); }
}

/// Sets the color of the icon used in a button.
/// Useful for overriding the icon color to something
/// other than the button text.
/// @example .class-name { @include icon-color($palette-primary); }
/// @arg {string} $color - a color value to be used as the color of the icon.
@mixin icon-color($color) {
	@if (lightness($color) > $lightness-tolerance ) {
		color: $palette-dark;
	}
	@else {
		color: $palette-light;
	}
}